package com.example.demo;

import java.util.ArrayList;
import java.util.List;

public class CurrentSpendingStats {
    private double monthBudget;
    private double currentSpending;
    private List<Expense> expenses = new ArrayList<>();

    public double getMonthBudget() {
        return monthBudget;
    }

    public void setMonthBudget(double monthBudget) {
        this.monthBudget = monthBudget;
    }

    public double getCurrentSpending() {


        return currentSpending;
    }

    public void setCurrentSpending(double currentSpending) {
        this.currentSpending = currentSpending;
    }

    public List<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }
}
