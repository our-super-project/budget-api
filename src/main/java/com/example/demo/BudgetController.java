package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@RestController
public class BudgetController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/greeting")
    public String getGreeting() {
        return "Terekkkkk!";
    }

    @Autowired
    private UserService userService;

    @GetMapping("/budget/{year}/{month}")
    public Budget getBudget(@PathVariable int year, @PathVariable int month) {
        User user = userService.getAuthenticatedUser();
        List<Budget> budgets = jdbcTemplate.query("select * from budget where year=? and month=? and userId=? ", new Object[]{year, month, user.getId()},
                (row, count) -> {
                    int budgetId = row.getInt("id");
                    int budgetYear = row.getInt("year");
                    int budgetMonth = row.getInt("month");
                    double budgetMonthBudget = row.getDouble("monthBudget");

                    Budget budget = new Budget();
                    budget.setId(budgetId);
                    budget.setYear(budgetYear);
                    budget.setMonth(budgetMonth);
                    budget.setMonthBudget(budgetMonthBudget);

                    return budget;
                });
        return budgets.size() > 0 ? budgets.get(0) : null;
    }

    @PostMapping("/budget")
    public void addBudget(@RequestBody Budget budget) {
        User user = userService.getAuthenticatedUser();
        int count = jdbcTemplate.queryForObject("select count(*) from budget where year = ? and month = ? and userId = ?", new Object[]{budget.getYear(), budget.getMonth(), user.getId()}, Integer.class);
        if (count > 0) {
            jdbcTemplate.update("update budget set monthBudget = ? where year = ? and month = ? and userId=?", budget.getMonthBudget(), budget.getYear(), budget.getMonth(), user.getId());
        } else {
            jdbcTemplate.update("insert into budget (year, month, monthBudget, userId) values (?, ?, ?, ?)", budget.getYear(), budget.getMonth(), budget.getMonthBudget(), user.getId());
        }
    }

    @GetMapping("/expense/{year}/{month}")
    public CurrentSpendingStats getExpense(@PathVariable int year, @PathVariable int month) {
        User user = userService.getAuthenticatedUser();


        List<Expense> expenses = null;

        if (year == 0 || month == 0) {
            expenses = this.getExpenses();
        } else {

            LocalDate periodStart = LocalDate.of(year, month, 1);
            LocalDate periodEnd = periodStart.plusMonths(1).minusDays(1);

            expenses = jdbcTemplate.query("select * from expense where userId = ? and date >= ? and date <= ? order by date asc", new Object[]{user.getId(), periodStart, periodEnd},

                    (row, count) -> {
                        int expenseId = row.getInt("id");
                        Date expenseDate = row.getDate("date");
                        double expenseValue = row.getDouble("value");
                        String expenseDescription = row.getString("description");
                        String expenseCategory = row.getString("category");

                        Expense expense = new Expense();
                        expense.setId(expenseId);
                        expense.setDate(expenseDate);
                        expense.setValue(expenseValue);
                        expense.setDescription(expenseDescription);
                        expense.setCategory(expenseCategory);


                        return expense;
                    });
        }

        Budget budget = this.getBudget(year, month);

        CurrentSpendingStats spendingStats = new CurrentSpendingStats();
        if (budget != null) {
            spendingStats.setMonthBudget(budget.getMonthBudget());
        }

        double sum = 0;
        for (Expense currentExpense : expenses) {
            sum = sum + currentExpense.getValue();
        }
        spendingStats.setCurrentSpending(sum);
        spendingStats.setExpenses(expenses);

        return spendingStats;
    }

    @PostMapping("/expense")
    public void addExpense(@RequestBody Expense expense){
        User user = userService.getAuthenticatedUser();

        jdbcTemplate.update("insert into expense (date, value, description, category, userId) values (?, ?, ?, ?, ?)", expense.getDate(), expense.getValue(), expense.getDescription(), expense.getCategory(), user.getId());

    }
    @PutMapping("/expense")
    public void editExpense(@RequestBody Expense expense){
        User user = userService.getAuthenticatedUser();

        jdbcTemplate.update("update expense set date = ?, value =?, description = ?, category =? where id =? and userId = ?", expense.getDate(), expense.getValue(),expense.getDescription(),expense.getCategory(),expense.getId(), user.getId());
    }
    @DeleteMapping("/expense/{id}")
    public void deleteExpense(@PathVariable int id){
        jdbcTemplate.update("delete from expense where id=?", id);

    }


   @GetMapping("/expense/{id}")
   public Expense getExpenseUser (@PathVariable int id){

        User user = userService.getAuthenticatedUser();

       Expense result =jdbcTemplate.queryForObject("select * from expense where id =? and userId = ?", new Object[] {id, user.getId()},
               (row, count)->{
                   int expenseId = row.getInt("id");
                   Date expenseDate = row.getDate("date");
                   double expenseValue = row.getDouble("value");
                   String expenseDescription = row.getString("description");
                   String expenseCategory = row.getString("category");


                   Expense expense = new Expense();
                   expense.setId(expenseId);
                   expense.setDate(expenseDate);
                   expense.setValue(expenseValue);
                   expense.setDescription(expenseDescription);
                   expense.setCategory(expenseCategory);


                   return expense;
               });
       return result;
   }




    @GetMapping("/expenses")
    public List<Expense> getExpenses(){
        User user = userService.getAuthenticatedUser();
        List<Expense> expenses = jdbcTemplate.query("select * from expense where userId = ?",new Object[]{user.getId()},
                (row, count) -> {
                    int expenseId = row.getInt("id");
                    Date expenseDate = row.getDate("date");
                    double expenseValue = row.getDouble("value");
                    String expenseDescription = row.getString("description");
                    String expenseCategory = row.getString("category");
                    int expenseUserId = row.getInt("userId");

                    Expense expense = new Expense();
                    expense.setId(expenseId);
                    expense.setDate(expenseDate);
                    expense.setValue(expenseValue);
                    expense.setDescription(expenseDescription);
                    expense.setCategory(expenseCategory);
                    expense.setUserId(expenseUserId);

                    return expense;
                }        );
        return expenses;

    }




}
