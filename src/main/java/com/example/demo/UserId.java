//package com.example.demo;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//
//import java.sql.Date;
//
//public class UserId {
//    @Autowired
//    private JdbcTemplate jdbcTemplate;
//
//    @GetMapping("/expense/{id}/{userId}")
//    public Expense getExpense (@PathVariable int id, @PathVariable int userId){
//
//        Expense result =jdbcTemplate.queryForObject("select * from expense where id =? and userId = ?", new Object[] {id, userId},
//                (row, count)->{
//                    int expenseId = row.getInt("id");
//                    Date expenseDate = row.getDate("date");
//                    double expenseValue = row.getDouble("value");
//                    String expenseDescription = row.getString("description");
//                    String expenseCategory = row.getString("category");
//
//                    Expense expense = new Expense();
//                    expense.setId(expenseId);
//                    expense.setDate(expenseDate);
//                    expense.setValue(expenseValue);
//                    expense.setDescription(expenseDescription);
//                    expense.setCategory(expenseCategory);
//
//                    return expense;
//                });
//        return result;
//    }
//
//}
