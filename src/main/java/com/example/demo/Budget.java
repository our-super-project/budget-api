package com.example.demo;


public class Budget {
    private int id;
    private int year;
    private int month;
    private double monthBudget;
    private double currentSpending;
    private int userId;

    public int getUserId() {
        return userId;
    }
    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getCurrentSpending() {
        return currentSpending;
    }
    public void setCurrentSpending(double currentSpending) {
        this.currentSpending = currentSpending;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public double getMonthBudget() {
        return monthBudget;
    }

    public void setMonthBudget(double monthBudget) {
        this.monthBudget = monthBudget;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

}
