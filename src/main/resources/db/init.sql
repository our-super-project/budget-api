-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for budget
DROP DATABASE IF EXISTS `budget`;
CREATE DATABASE IF NOT EXISTS `budget` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `budget`;

-- Dumping structure for table budget.budget
DROP TABLE IF EXISTS `budget`;
CREATE TABLE IF NOT EXISTS `budget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `monthBudget` double NOT NULL,
  `userId` int(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Dumping data for table budget.budget: ~14 rows (approximately)
/*!40000 ALTER TABLE `budget` DISABLE KEYS */;
INSERT INTO `budget` (`id`, `year`, `month`, `monthBudget`, `userId`) VALUES
	(1, 2019, 1, 1000, 2),
	(2, 2019, 2, 2400, 2),
	(3, 2019, 3, 1000, 2),
	(6, 2019, 4, 354, 2),
	(7, 2019, 8, 1240, 2),
	(8, 2019, 9, 1240, 2),
	(9, 2019, 5, 4444, 2),
	(10, 2019, 11, 666, 2),
	(11, 2019, 12, 7777, 2),
	(12, 2021, 4, 4444, 2),
	(13, 2019, 1, 1000, 3),
	(14, 2019, 2, 1500, 3),
	(15, 2019, 4, 500, 3),
	(16, 2018, 1, 1000, 3),
	(17, 2018, 2, 45666, 3);
/*!40000 ALTER TABLE `budget` ENABLE KEYS */;

-- Dumping structure for table budget.expense
DROP TABLE IF EXISTS `expense`;
CREATE TABLE IF NOT EXISTS `expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `value` double NOT NULL,
  `description` varchar(300) NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `userId` int(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_expense_user` (`userId`),
  CONSTRAINT `FK_expense_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

-- Dumping data for table budget.expense: ~37 rows (approximately)
/*!40000 ALTER TABLE `expense` DISABLE KEYS */;
INSERT INTO `expense` (`id`, `date`, `value`, `description`, `category`, `userId`) VALUES
	(2, '2019-01-01', 77, 'prisma', 'toidukulud', 3),
	(4, '2019-01-12', 15, 'taxi', 'transpordi kulud', 3),
	(12, '2019-02-04', 35, 'elekter', 'eluasemekulud', 3),
	(13, '2019-01-01', 5.5, 'food', 'transpordi kulud', 2),
	(15, '2019-01-01', 19, 'bills', 'eluasemekulud', 2),
	(16, '2019-02-04', 45, 'kleit', 'muud kulud', 2),
	(18, '2019-02-02', 15, 'jook', 'toidukulud', 2),
	(20, '2019-02-02', 3.5, 'pilet', 'transpordi kulud', 2),
	(26, '2019-02-04', 24, 'kino', 'muud kulud', 2),
	(29, '2019-01-01', 45, 'prisma', 'toidukulud', 2),
	(31, '2019-01-01', 5, 'pilet', 'transpordi kulud', 2),
	(32, '2019-02-07', 78, 'food', 'transpordi kulud', 2),
	(35, '2019-02-07', 45, 'selver', 'toidukulud', 2),
	(36, '2019-02-02', 4.5, 'prisma', 'toidukulud', 2),
	(39, '2019-02-04', 4, 'pilet', 'transpordi kulud', 2),
	(41, '2019-01-02', 123, 'tantsud', 'muud kulud', 2),
	(42, '2019-01-02', 21, 'taxi', 'transpordi kulud', 3),
	(44, '2019-01-18', 123, 'rimi', 'toidukulud', 2),
	(45, '2019-01-16', 12, 'kohvik', 'muud kulud', 2),
	(47, '2019-01-30', 100, 'rimi', 'toidukulud', 2),
	(48, '2019-02-05', 153, 'food', 'toidukulud', 2),
	(52, '2019-02-10', 123, 'küte', 'eluasemekulud', 2),
	(53, '2019-02-11', 12, 'food', 'toidukulud', 2),
	(54, '2019-02-11', 12, 'food', 'toidukulud', 3),
	(57, '2019-02-11', 2.5, 'tramm', 'transpordi kulud', 2),
	(58, '2019-02-11', 25, 'kindad', 'muud kulud', 2),
	(61, '2019-02-12', 12, 'takso', 'transpordi kulud', 2),
	(62, '2019-02-12', 4.5, 'takso', 'transpordi kulud', 3),
	(63, '2019-02-12', 1, 'pulgakomm', 'toidukulud', 3),
	(64, '2019-02-12', 1, 'komm', 'toidukulud', 3),
	(65, '2019-02-12', 12, 'pilet', 'transpordi kulud', 3),
	(66, '2019-02-12', 12, 'kino', 'muud kulud', 3),
	(67, '2019-01-02', 100, 'kleit', 'eluasemekulud', 3),
	(68, '2019-02-11', 13, 'pilet', 'transpordi kulud', 3),
	(69, '2019-02-09', 12, 'pilet', 'transpordi kulud', 2),
	(70, '2019-02-12', 45, 'bensiin', 'transpordi kulud', 2),
	(71, '2019-02-13', 9, 'kringel', 'toidukulud', 3),
	(72, '2019-02-14', 34, 'kringel', 'toidukulud', 2);
/*!40000 ALTER TABLE `expense` ENABLE KEYS */;

-- Dumping structure for table budget.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table budget.user: ~2 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`) VALUES
	(2, 'Veronika', '$2a$10$6ztnlRU3inMofQ03Pg/oB.MWfHEq7L3DE1NEKnXaH7Qg5sOXO.uI2'),
	(3, 'Katrin', '$2a$10$64XVXTlZBAvhPOLcD73NKOL48sbXVsUo34C/KjxQuFoM1DnWGHIr.');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
